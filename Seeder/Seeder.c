// Seeder.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

TCHAR *randstring(size_t length) {
	static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	TCHAR *randomString = NULL;

	if (length) {
		randomString = malloc(sizeof(TCHAR) * (length + 1) + 10);
		if (randomString) {
			for (int n = 0; n < length; n++) {
				int key = rand() % (int)(sizeof(charset) - 1);
				randomString[n] = charset[key];
			}
			randomString[length] = '\0';
		}
	}
	return randomString;
}

void DeleteDirectoryFiles(TCHAR *directory) {
	HANDLE handle;
	WIN32_FIND_DATA file;

	TCHAR searchString[100];
	wcscpy(searchString, directory);
	wcscat(searchString, L"*");
	TCHAR filepath[MAX_PATH];

	handle = FindFirstFile(searchString, &file);
	searchString[wcslen(searchString) - 1] = 0;

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			wcscpy(filepath, directory);
			wcscat(filepath, file.cFileName);
			DeleteFile(filepath);
		}
	}
}

int main()
{
	int status;
	int i;
	char usableString[MAX_PATH + 80], usableString2[MAX_PATH + 80];
	FILE *fp;
	TCHAR *random, *random2;
	TCHAR base[100] = L"C:\\ProggerTests", Creater[10] = L"\\Creater", Deleter[10] = L"\\Deleter", Mover[10] = L"\\Mover", Reader[10] = L"\\Reader", 
		Updater[10] = L"\\Updater", MultiOp[10] = L"\\MultiOp", useString[MAX_PATH + 80], useString2[MAX_PATH + 80], combined[MAX_PATH * 2], combined2[MAX_PATH * 2], FullMultiOp[13] = L"\\FullMultiOp";
	status = CreateDirectoryW(base, NULL);

	printf("Creating Creater Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, Creater);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old Creater Files\r\n\r\n");
	DeleteDirectoryFiles(useString);

	printf("Creating Deleter Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, Deleter);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old Deleter Files\r\n\r\n");
	DeleteDirectoryFiles(useString);

	printf("Creating Deleter Files\r\n\r\n");
	for (i = 0; i < 20000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		fp = fopen(usableString, "w+");
		random2 = randstring(50);
		fprintf(fp, "%ws", random2);
		fclose(fp);
		free(random);
		free(random2);
	}

	printf("Creating Mover Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, Mover);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old Mover Files\r\n\r\n");
	DeleteDirectoryFiles(useString);
	
	printf("Creating Mover Files\r\n\r\n");
	for (i = 0; i < 20000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		fp = fopen(usableString, "w+");
		random2 = randstring(50);
		fprintf(fp, "%ws", random2);
		fclose(fp);
		free(random);
		free(random2);
	}

	printf("Creating Reader Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, Reader);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old Reader Files\r\n\r\n");
	DeleteDirectoryFiles(useString);

	printf("Creating Reader Files\r\n\r\n");
	for (i = 0; i < 20000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		fp = fopen(usableString, "w+");
		random2 = randstring(50);
		fprintf(fp, "%ws", random2);
		fclose(fp);
		free(random);
		free(random2);
	}

	printf("Creating Updater Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, Updater);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old Updater Files\r\n\r\n");
	DeleteDirectoryFiles(useString);

	printf("Creating Updater Files\r\n\r\n");
	for (i = 0; i < 20000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		fp = fopen(usableString, "w+");
		fclose(fp);
		free(random);
	}

	printf("Creating MultiOp Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, MultiOp);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old MultiOp Files\r\n\r\n");
	DeleteDirectoryFiles(useString);

	printf("Creating MultiOp Files\r\n\r\n");
	for (i = 0; i < 20000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		fp = fopen(usableString, "w+");
		random2 = randstring(50);
		fprintf(fp, "%ws", random2);
		fclose(fp);
		free(random);
		free(random2);
	}

	printf("Creating FullMultiOp Directory\r\n\r\n");
	wcscpy(useString, base);
	wcscat(useString, FullMultiOp);
	status = CreateDirectory(useString, NULL);
	wcscat(useString, L"\\");
	printf("Removing old FullMultiOp Files\r\n\r\n");
	DeleteDirectoryFiles(useString);

	printf("Creating FullMultiOp Files\r\n\r\n");
	for (i = 0; i < 20000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		fp = fopen(usableString, "w+");
		random2 = randstring(50);
		fprintf(fp, "%ws", random2);
		fclose(fp);
		free(random);
		free(random2);
	}

	printf("Creating FullMultiOp Directories\r\n\r\n");
	for (i = 0; i < 1000; i++) {
		memset(combined, 0, MAX_PATH);
		memset(usableString, 0, MAX_PATH);
		wcscpy(combined, useString);

		random = randstring(80);
		wcscat(combined, random);
		wcstombs(usableString, combined, MAX_PATH);
		CreateDirectoryA(usableString, NULL);
		wcscat(combined, L"\\");
		wcscpy(useString2, combined);
		for (int j = 0; j < 50; j++) {
			memset(combined2, 0, MAX_PATH * 2);
			memset(usableString2, 0, MAX_PATH * 2);
			wcscpy(combined2, useString2);

			random = randstring(80);
			wcscat(random, L".txt\0");
			wcscat(combined2, random);
			wcstombs(usableString2, combined2, MAX_PATH * 2);
			fp = fopen(usableString2, "w+");
			random2 = randstring(50);
			fprintf(fp, "%ws", random2);
			fclose(fp);
			free(random);
			free(random2);
		}
	}
	return 0;
}

